package com.example.pwalaunchtest

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Context.NOTIFICATION_SERVICE
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat

class PWAPlayerStatusReceiver: BroadcastReceiver() {

    private val TAG = PWAPlayerStatusReceiver::class.simpleName

    companion object {
        const val ACTION_PWA_ADDED = "foundation.e.pwaplayer.PWA_ADDED"
        const val ACTION_PWA_REMOVED = "foundation.e.pwaplayer.PWA_REMOVED"
        const val ACTION_PWA_CHANGED = "foundation.e.pwaplayer.PWA_CHANGED"

        const val ACTION_VIEW_PWA = "foundation.e.blisslauncher.VIEW_PWA"
        const val PWA_ID = "PWA_ID"
        const val URL = "URL"
    }

    override fun onReceive(context: Context?, intent: Intent?) {

        val action = intent?.action
        val url = intent?.getStringExtra(URL)
        val pwaId = intent?.getLongExtra(PWA_ID, 0)

        Log.d(TAG, "received action: $action");
        Log.d(TAG, "received URL: $url");
        Log.d(TAG, "received SHORTCUT_ID: ${intent?.getStringExtra("SHORTCUT_ID")}");
        Log.d(TAG, "received PWA_ID: $pwaId");

        if (action == ACTION_PWA_ADDED) {
            Log.d(TAG, "launching PWA")
            val launchIntent = Intent(ACTION_VIEW_PWA).apply {
                data = Uri.parse(url)
                putExtra(PWA_ID, pwaId)
                addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                addFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT or Intent.FLAG_ACTIVITY_RETAIN_IN_RECENTS)
            }

            /*
             * Android 10 does not allow direct start of activities from broadcast.
             */
            val notificationManager = context!!.getSystemService(NOTIFICATION_SERVICE) as NotificationManager
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                notificationManager.createNotificationChannel(NotificationChannel("channel", "channel", NotificationManager.IMPORTANCE_DEFAULT))
            }

            val notification = NotificationCompat.Builder(context, "channel")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("PWA installed")
                .setContentIntent(PendingIntent.getActivity(context, 100, launchIntent, 0))
                .build()
            notificationManager.notify(100, notification)
        }

    }
}